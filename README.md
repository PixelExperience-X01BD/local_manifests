<div align="center">
    <img width="572" src="./teaser@2x.jpg">
</div>


1. **Sync ROM-side code**

```sh
# Initialize local repository
mkdir pe-pie
cd pe-pie
repo init -u https://github.com/PixelExperience/manifest -b pie

# Sync
repo sync -c -j8 --force-sync --no-clone-bundle --no-tags
```

2. **Sync device-side code**

```sh
# Initialize local manifests
cd .repo
git clone git@gitlab.com:PixelExperience-X01BD/local_manifests.git
cd ../

# Sync
repo sync -c -j8 --force-sync --no-clone-bundle --no-tags
```

3. **Build**

```sh
# Set up environment
. build/envsetup.sh

# Choose a target
lunch aosp_X01BD-userdebug

# Build the code
mka bacon -j8
```
